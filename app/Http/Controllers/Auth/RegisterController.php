<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\User;
use App\Models\Customer;
use App\Vendor;

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function show($type)
    {
        if ($type == 'store')
            $isStore = true;
        else if ($type == 'customer')
            $isStore = false;
        else
            return redirect()->route('homepage');

        return view('auth.register', compact('isStore'));
    }
    
    public function register(Request $request)
    {
        $rules = [
            'name'      => 'required|string|max:200',
            'email'     => 'required|string|email|max:200|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ];

        $messages = [
            'email.unique' => 'El :attribute ingresado ya está existe.',
            'password.confirmed' => 'Las contraseñas ingresadas no coinciden.',
        ];

        $this->validate($request, $rules, $messages);
        
        $user = $this->create($request);

        Auth::login($user);

        /*
        if ($response = $this->registered($request, $user)) {
            return redirect()->route('customer.index');
        }
        */

        return redirect()->route('customer.index');
    }
    
    private function create(Request $request)
    {
        // set slug
        $slug = Str::slug($request->input('name'));
        $userSlug = User::where('slug', $slug)->get();

        if ($userSlug->count() > 0) {
            $newSlug = $slug.($userSlug->count() + 1);
        }
        else {
            $newSlug = $slug;
        }

        // set user type
        $hasBecomeStore = (bool) $request->input('become_store');
        $userType = $hasBecomeStore ? 'Vendor' : 'Customer';

        // create user
        $user = new User();
        $user->name = $request->input('name');
        $user->email= $request->input('email');
        $user->slug = $newSlug;
        $user->password = Hash::make($request->input('password'));
        $user->email_verified_at = Carbon::now();
        $user->user_type = $userType;
        $user->save();

        $user->assignGroup(2);
        
        // create as customer
        $customer = new Customer;
        $customer->user_id = $user->id;
        $customer->save();

        // create as store
        if ($hasBecomeStore)
        {
            $store = new Vendor;
            $store->user_id = $user->id;
            $store->name = $user->name;
            $store->email = $user->email;
            $store->slug = $user->slug;
            $store->save();
        }

        return $user;
    }
}
