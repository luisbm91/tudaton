<div class="ps-panel--sidebar" id="menu-mobile">
  <div class="ps-panel__header">
    <h3>@translate(Menu)</h3>
  </div>
  <div class="ps-panel__content">
    <ul class="menu--mobile">
      <li><a href="{{ route('all.product') }}">@translate(All Products)</a></li>
      <li><a href="{{ route('vendor.shops') }}">@translate(All Shops)</a></li>
      <li><a href="{{ route('brands') }}">@translate(All Brands)</a></li>
      <li><a href="{{ route('customer.campaigns.index') }}">@translate(Campaigns)</a></li>
      <li>
        <a href="{{ route('register', ['type' => 'store']) }}">
          @lang('header.top-be_a_seller')
        </a>
      </li>
    </ul>
  </div>
</div>
