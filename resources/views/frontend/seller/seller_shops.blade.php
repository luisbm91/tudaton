@extends('frontend.master')

@section('title') @translate(Shops) @endsection

@section('content')
  <div class="ps-breadcrumb">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="{{ route('homepage') }}">@translate(Home)</a></li>
        <li>@translate(Shops)</li>
      </ul>
    </div>
  </div>
    
  <div class="ps-section--shopping ps-whishlist">
    <div class="container">
      <div class="ps-section__header">
        <h1>@translate(All Shop)</h1>
      </div>
      <div class="ps-section__content">
        <div class="container">
          <form class="ps-form--newsletter" action="javascript:void()" method="post">
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                <div class="ps-form__right">
                  <div class="form-group--nest">
                    <input class="form-control"
                      type="text" id="myInput"
                      placeholder="@translate(Search shop here)">
                    <button class="ps-btn">@translate(Search)</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div class="container" id="myShop">
          <div class="row t-mt-30">
            @forelse($usersStore as $userStore)
              @php
                $store = App\Vendor::where('user_id', $userStore->id)->first();
              @endphp

              @if ($store)
                <div class="col-md-3 col-xl-3 t-mb-30">
                  <a href="{{ route('vendor.shop', $store->slug) }}" class="product-card">
                    @if (empty($store->shop_logo))
                      <span class="product-card__img-wrapper">
                        <img src="{{ asset('vendor-store.jpg') }}"
                          alt="{{ $store->shop_name }}"
                          class="img-fluid mx-auto" />
                      </span>
                    @else
                      <span class="product-card__img-wrapper">
                        <img src="{{ filePath($store->shop_logo) }}"
                          alt="{{ $store->shop_name }}"
                          class="img-fluid mx-auto" />
                      </span>
                    @endif
                    <span class="product-card__body">
                      <span class="product-card__title text-center h3 text-capitalize font-weight-bold">
                        {{ $store->shop_name }}
                      </span>
                    </span>
                  </a>
                </div>
              @endif
            @empty
              <div class="col-md-12">
                <img src="{{ asset('shop-not-found.png') }}" class="img-fluid" alt="#shop-not-found">
              </div>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
