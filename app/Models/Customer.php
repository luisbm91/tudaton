<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $fillable = [
      'user_id',
      'address',//nullable
    ];

    public $timestamps = false;
    
}
