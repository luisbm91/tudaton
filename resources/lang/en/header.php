<?php

return [
    'top-be_a_seller' => 'I want to sell my Products',
    'top-login_register' => 'Login / Sign up',
    'sidebar-categories' => 'Categories',
    'search-input' => 'I\'m looking for ...',
    'search-button' => 'Search',
    'search-select-product' => 'Products List',
    'search-select-shop' => 'Stores List',
    'navbar-products' => 'Products',
    'navbar-stores' => 'Stores',
    'navbar-brands' => 'Brands',
    'navbar-campaigns' => 'Campaigns',
];