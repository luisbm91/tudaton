<?php

return [
    'top-be_a_seller' => 'Quiero vender mis Productos',
    'top-login_register' => 'Ingresa / Regístrate',
    'sidebar-categories' => 'Categorias',
    'search-input' => 'Estoy buscando ...',
    'search-button' => 'Buscar',
    'search-select-product' => 'En Productos',
    'search-select-shop' => 'En Tiendas',
    'navbar-products' => 'Productos',
    'navbar-stores' => 'Tiendas',
    'navbar-brands' => 'Marcas',
    'navbar-campaigns' => 'Campañas',
];