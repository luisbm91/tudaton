<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'productId'=>$this->product_id,
            'name'=>$this->name,
            'image'=>$this->image,
            'slug'=>$this->slug,
            'sku'=>$this->sku,
            'discount'=>$this->discount,
            'discountHave'=>$this->discountHave,
            'price'=>$this->price
        ];
    }
}
