@extends('frontend.master')

@section('title')
  Mi perfil
@endsection

@section('content')
  <div class="ps-vendor-dashboard pro">
    <div class="container">

      <x-admin-navbar/>

      <form action="{{route('customer.update')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 ">
            <figure class="ps-block--vendor-status w-300 text-center">
              @if (Str::substr($user->avatar, 0, 7) == 'uploads')
                <img src="{{ filePath($user->avatar) }}" alt="{{ $user->name }}" class="w-70 rounded-circle">
              @else
                <img src="{{ asset($user->avatar) }}" alt="{{ $user->name }}" class="w-70 rounded-circle">
              @endif
            </figure>
          </div>
          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
            <figure class="ps-block--vendor-status">
              <figcaption>Mi Perfil</figcaption>
              <input type="hidden" name="slug" value="{{ $user->slug }}">
              @if($errors->has('name'))
                  <div class="error text-danger">{{ $errors->first('name') }}</div>
              @endif
              <div class="form-group">
                  <input class="form-control" type="text" placeholder="@translate(Name)" name="name"
                          value="{{ $user->name ?? '' }}">
              </div>
              <div class="form-group">
                  <input class="form-control" type="email" placeholder="@translate(Email address)"
                          name="email" value="{{ $user->email ?? '' }}" disabled>
              </div>
              <div class="form-group">
                  <input class="form-control" type="number" placeholder="@translate(Contact number)"
                          name="phn_no" value="{{ $user->tel_number ?? '' }}">
              </div>

              <div class="form-group">
                  <input class="form-control" type="text" placeholder="@translate(Nationality)"
                          name="nationality" value="{{ $user->nationality ?? ''}}">
              </div>


              <div class="form-group">
                  <textarea class="form-control" placeholder="@translate(Your address)"
                            name="address">{{ $customers->address ?? '' }}</textarea>
              </div>


              @if($errors->has('avatar'))
                  <div class="error text-danger">{{ $errors->first('avatar') }}</div>
              @endif
              <div class="form-group">
                  <input type="hidden" name="oldAvatar" value="{{ $user->avatar ?? '' }}">
                  <input class="form-control pt-3" type="file" name="avatar">
              </div>
              <h4>Confirm your password, for profile update.</h4>
              <div class="form-group">
                  <input class="form-control" type="password" placeholder="@translate(Password)"
                          name="password">
              </div>
              @if($errors->has('password'))
                  <div class="error text-danger">{{ $errors->first('password') }}</div>
              @endif
              <div class="form-group">
                  <input class="form-control" type="password" placeholder="@translate(Confirm password)"
                          name="password_confirmation">
              </div>
              <button type="submit" class="ps-btn">
                Grabar cambios
              </button>
            </figure>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop
