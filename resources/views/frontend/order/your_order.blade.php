@extends('frontend.master')

@section('title')@translate(Your order) @endsection

@section('content')
    <div class="ps-vendor-dashboard pro">
        <div class="container">
          
          <x-admin-navbar/>
          
            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <figure class="ps-block--vendor-status">
                        <figcaption>@translate(Your Orders)({{ $orders->count() }})</figcaption>

                        <table class="table ps-table ps-table--vendor">
                            <thead>
                            <tr>
                                <th>@translate(Order ID)</th>
                                <th>@translate(Order Type)</th>
                                <th>@translate(Totals)</th>
                                <th>@translate(Actions)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($orders as $order)
                                <tr>
                                    <td>
                                        <a href="{{ route('customer.order_details', $order->order_number) }}">{{ $order->order_number }}</a>
                                        <p>{{ $order->created_at->format('M d, Y') }}</p>
                                    </td>

                                    <td>
                                        <p>{{ $order->payment_type }}</p>
                                    </td>
                                    <td>
                                        <p>{{ formatPrice($order->pay_amount) }}</p>
                                    </td>
                                    <td>
                                        <a href="{{ route('customer.order_details', $order->order_number) }}"
                                           class="btn btn-warning text-dark">Details</a>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-center">
                                    <td colspan="4">
                                        @translate(No Order Found)
                                    </td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </figure>
                </div>

            </div>
            {{-- Orders content goes here::END --}}
        </div>
    </div>
@stop
