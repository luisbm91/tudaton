// Guest wishlist
var wish_list = new Array();
var productId = new Array();
// Guest wishlist::END

(function ($) {
    "use strict"

    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    }
    
    //code by Akash here
    var is_auth = $('.auth-check').val();

    if (is_auth == 1 && localStorage.getItem('guest_cart_items') === null) {
        cartList();
    } else {
        guestCartList();
    }

    if (is_auth == 1) {
        wishList();
    }

    compareList();
    //code by Akash ends here
    function parallax() {
        $('.bg--parallax').each(function () {
            var el = $(this),
                xpos = "50%",
                windowHeight = $(window).height();
            if (isMobile.any()) {
                $(this).css('background-attachment', 'scroll');
            } else {
                $(window).scroll(function () {
                    var current = $(window).scrollTop(),
                        top = el.offset().top,
                        height = el.outerHeight();
                    if (top + height < current || top > current + windowHeight) {
                        return;
                    }
                    el.css('backgroundPosition', xpos + " " + Math.round((top - current) * 0.2) + "px");
                });
            }
        });
    }

    function backgroundImage() {
        var databackground = $('[data-background]');
        databackground.each(function () {
            if ($(this).attr('data-background')) {
                var image_path = $(this).attr('data-background');
                $(this).css({
                    'background': 'url(' + image_path + ')'
                });
            }
        });
    }

    function siteToggleAction() {
        var navSidebar = $('.navigation--sidebar'),
            filterSidebar = $('.ps-filter--sidebar');
        $('.menu-toggle-open').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active')
            navSidebar.toggleClass('active');
            $('.ps-site-overlay').toggleClass('active');
        });

        $('.ps-toggle--sidebar').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $(this).toggleClass('active');
            $(this).siblings('a').removeClass('active');
            $(url).toggleClass('active');
            $(url).siblings('.ps-panel--sidebar').removeClass('active');
            $('.ps-site-overlay').toggleClass('active');
        });

        $('#filter-sidebar').on('click', function (e) {
            e.preventDefault();
            filterSidebar.addClass('active');
            $('.ps-site-overlay').addClass('active');
        });

        $('.ps-filter--sidebar .ps-filter__header .ps-btn--close').on('click', function (e) {
            e.preventDefault();
            filterSidebar.removeClass('active');
            $('.ps-site-overlay').removeClass('active');
        });

        $('body').on("click", function (e) {
            if ($(e.target).siblings(".ps-panel--sidebar").hasClass('active')) {
                $('.ps-panel--sidebar').removeClass('active');
                $('.ps-site-overlay').removeClass('active');
            }
        });
    }

    function subMenuToggle() {
        $('.menu--mobile .menu-item-has-children > .sub-toggle').on('click', function (e) {
            e.preventDefault();
            var current = $(this).parent('.menu-item-has-children')
            $(this).toggleClass('active');
            current.siblings().find('.sub-toggle').removeClass('active');
            current.children('.sub-menu').slideToggle(350);
            current.siblings().find('.sub-menu').slideUp(350);
            if (current.hasClass('has-mega-menu')) {
                current.children('.mega-menu').slideToggle(350);
                current.siblings('.has-mega-menu').find('.mega-menu').slideUp(350);
            }

        });
        $('.menu--mobile .has-mega-menu .mega-menu__column .sub-toggle').on('click', function (e) {
            e.preventDefault();
            var current = $(this).closest('.mega-menu__column')
            $(this).toggleClass('active');
            current.siblings().find('.sub-toggle').removeClass('active');
            current.children('.mega-menu__list').slideToggle(350);
            current.siblings().find('.mega-menu__list').slideUp(350);
        });
        var listCategories = $('.ps-list--categories');
        if (listCategories.length > 0) {
            $('.ps-list--categories .menu-item-has-children > .sub-toggle').on('click', function (e) {
                e.preventDefault();
                var current = $(this).parent('.menu-item-has-children')
                $(this).toggleClass('active');
                current.siblings().find('.sub-toggle').removeClass('active');
                current.children('.sub-menu').slideToggle(350);
                current.siblings().find('.sub-menu').slideUp(350);
                if (current.hasClass('has-mega-menu')) {
                    current.children('.mega-menu').slideToggle(350);
                    current.siblings('.has-mega-menu').find('.mega-menu').slideUp(350);
                }

            });
        }
    }

    function stickyHeader() {
        var header = $('.header'),
            scrollPosition = 0,
            checkpoint = 50;
        header.each(function () {
            if ($(this).data('sticky') === true) {
                var el = $(this);
                $(window).scroll(function () {

                    var currentPosition = $(this).scrollTop();
                    if (currentPosition > checkpoint) {
                        el.addClass('header--sticky');
                    } else {
                        el.removeClass('header--sticky');
                    }
                });
            }
        })

        var stickyCart = $('#cart-sticky');
        if (stickyCart.length > 0) {
            $(window).scroll(function () {
                var currentPosition = $(this).scrollTop();
                if (currentPosition > checkpoint) {
                    stickyCart.addClass('active');
                } else {
                    stickyCart.removeClass('active');
                }
            });
        }
    }

    function owlCarouselConfig() {
        var target = $('.owl-slider');
        if (target.length > 0) {
            target.each(function () {
                var el = $(this),
                    dataAuto = el.data('owl-auto'),
                    dataLoop = el.data('owl-loop'),
                    dataSpeed = el.data('owl-speed'),
                    dataGap = el.data('owl-gap'),
                    dataNav = el.data('owl-nav'),
                    dataDots = el.data('owl-dots'),
                    dataAnimateIn = (el.data('owl-animate-in')) ? el.data('owl-animate-in') : '',
                    dataAnimateOut = (el.data('owl-animate-out')) ? el.data('owl-animate-out') : '',
                    dataDefaultItem = el.data('owl-item'),
                    dataItemXS = el.data('owl-item-xs'),
                    dataItemSM = el.data('owl-item-sm'),
                    dataItemMD = el.data('owl-item-md'),
                    dataItemLG = el.data('owl-item-lg'),
                    dataItemXL = el.data('owl-item-xl'),
                    dataNavLeft = (el.data('owl-nav-left')) ? el.data('owl-nav-left') : "<i class='icon-chevron-left'></i>",
                    dataNavRight = (el.data('owl-nav-right')) ? el.data('owl-nav-right') : "<i class='icon-chevron-right'></i>",
                    duration = el.data('owl-duration'),
                    datamouseDrag = (el.data('owl-mousedrag') == 'on') ? true : false;
                if (target.children('div, span, a, img, h1, h2, h3, h4, h5, h5').length >= 2) {
                    el.owlCarousel({
                        animateIn: dataAnimateIn,
                        animateOut: dataAnimateOut,
                        margin: dataGap,
                        autoplay: dataAuto,
                        autoplayTimeout: dataSpeed,
                        autoplayHoverPause: true,
                        loop: dataLoop,
                        nav: dataNav,
                        mouseDrag: datamouseDrag,
                        touchDrag: true,
                        autoplaySpeed: duration,
                        navSpeed: duration,
                        dotsSpeed: duration,
                        dragEndSpeed: duration,
                        navText: [dataNavLeft, dataNavRight],
                        dots: dataDots,
                        items: dataDefaultItem,
                        responsive: {
                            0: {
                                items: dataItemXS
                            },
                            480: {
                                items: dataItemSM
                            },
                            768: {
                                items: dataItemMD
                            },
                            992: {
                                items: dataItemLG
                            },
                            1200: {
                                items: dataItemXL
                            },
                            1680: {
                                items: dataDefaultItem
                            }
                        }
                    });
                }

            });
        }
    }

    function masonry($selector) {
        var masonry = $($selector);
        if (masonry.length > 0) {
            if (masonry.hasClass('filter')) {
                masonry.imagesLoaded(function () {
                    masonry.isotope({
                        columnWidth: '.grid-sizer',
                        itemSelector: '.grid-item',
                        isotope: {
                            columnWidth: '.grid-sizer'
                        },
                        filter: "*"
                    });
                });
                var filters = masonry.closest('.masonry-root').find('.ps-masonry-filter > li > a');
                filters.on('click', function (e) {
                    e.preventDefault();
                    var selector = $(this).attr('href');
                    filters.find('a').removeClass('current');
                    $(this).parent('li').addClass('current');
                    $(this).parent('li').siblings('li').removeClass('current');
                    $(this).closest('.masonry-root').find('.ps-masonry').isotope({
                        itemSelector: '.grid-item',
                        isotope: {
                            columnWidth: '.grid-sizer'
                        },
                        filter: selector
                    });
                    return false;
                });
            } else {
                masonry.imagesLoaded(function () {
                    masonry.masonry({
                        columnWidth: '.grid-sizer',
                        itemSelector: '.grid-item'
                    });
                });
            }
        }
    }


    function slickConfig() {
        var product = $('.ps-product--detail');
        if (product.length > 0) {
            var primary = product.find('.ps-product__gallery'),
                second = product.find('.ps-product__variants'),
                vertical = product.find('.ps-product__thumbnail').data('vertical');
            primary.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                asNavFor: '.ps-product__variants',
                fade: true,
                dots: false,
                infinite: false,
                arrows: primary.data('arrow'),
                prevArrow: "<a href='#'><i class='fa fa-angle-left'></i></a>",
                nextArrow: "<a href='#'><i class='fa fa-angle-right'></i></a>",
            });
            second.slick({
                slidesToShow: second.data('item'),
                slidesToScroll: 1,
                infinite: false,
                arrows: second.data('arrow'),
                focusOnSelect: true,
                prevArrow: "<a href='#'><i class='fa fa-angle-up'></i></a>",
                nextArrow: "<a href='#'><i class='fa fa-angle-down'></i></a>",
                asNavFor: '.ps-product__gallery',
                vertical: vertical,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            arrows: second.data('arrow'),
                            slidesToShow: 4,
                            vertical: false,
                            prevArrow: "<a href='#'><i class='fa fa-angle-left'></i></a>",
                            nextArrow: "<a href='#'><i class='fa fa-angle-right'></i></a>"
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            arrows: second.data('arrow'),
                            slidesToShow: 4,
                            vertical: false,
                            prevArrow: "<a href='#'><i class='fa fa-angle-left'></i></a>",
                            nextArrow: "<a href='#'><i class='fa fa-angle-right'></i></a>"
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3,
                            vertical: false,
                            prevArrow: "<a href='#'><i class='fa fa-angle-left'></i></a>",
                            nextArrow: "<a href='#'><i class='fa fa-angle-right'></i></a>"
                        }
                    },
                ]
            });
        }
    }

    function tabs() {
        $('.ps-tab-list  li > a ').on('click', function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $(this).closest('li').siblings('li').removeClass('active');
            $(this).closest('li').addClass('active');
            $(target).addClass('active');
            $(target).siblings('.ps-tab').removeClass('active');
        });
        $('.ps-tab-list.owl-slider .owl-item a').on('click', function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $(this).closest('.owl-item').siblings('.owl-item').removeClass('active');
            $(this).closest('.owl-item').addClass('active');
            $(target).addClass('active');
            $(target).siblings('.ps-tab').removeClass('active');
        });
    }

    function rating() {
        $('select.ps-rating').each(function () {
            var readOnly;
            if ($(this).attr('data-read-only') == 'true') {
                readOnly = true
            } else {
                readOnly = false;
            }
            $(this).barrating({
                theme: 'fontawesome-stars',
                readonly: readOnly,
                emptyValue: '0'
            });
        });
    }

    /*
    function productLightbox() {
        var product = $('.ps-product--detail');
        if (product.length > 0) {
            $('.ps-product__gallery').lightGallery({
                selector: '.item a',
                thumbnail: true,
                share: false,
                fullScreen: false,
                autoplay: false,
                autoplayControls: false,
                actualSize: false
            });
            if (product.hasClass('ps-product--sticky')) {
                $('.ps-product__thumbnail').lightGallery({
                    selector: '.item a',
                    thumbnail: true,
                    share: false,
                    fullScreen: false,
                    autoplay: false,
                    autoplayControls: false,
                    actualSize: false
                });
            }
        }
        $('.ps-gallery--image').lightGallery({
            selector: '.ps-gallery__item',
            thumbnail: true,
            share: false,
            fullScreen: false,
            autoplay: false,
            autoplayControls: false,
            actualSize: false
        });
        $('.ps-video').lightGallery({
            thumbnail: false,
            share: false,
            fullScreen: false,
            autoplay: false,
            autoplayControls: false,
            actualSize: false
        });
    }
    */

    function backToTop() {
        var scrollPos = 0;
        var element = $('#back2top');
        $(window).scroll(function () {
            var scrollCur = $(window).scrollTop();
            if (scrollCur > scrollPos) {
                // scroll down
                if (scrollCur > 500) {
                    element.addClass('active');
                } else {
                    element.removeClass('active');
                }
            } else {
                // scroll up
                element.removeClass('active');
            }

            scrollPos = scrollCur;
        });

        element.on('click', function () {
            $('html, body').animate({
                scrollTop: '0px'
            }, 800);
        });
    }

    function filterSlider() {
        var el = $('.ps-slider');
        var min = el.siblings().find('.ps-slider__min');
        var max = el.siblings().find('.ps-slider__max');
        var defaultMinValue = el.data('default-min');
        var defaultMaxValue = el.data('default-max');
        var maxValue = el.data('max');
        var step = el.data('step');
        if (el.length > 0) {
            el.slider({
                min: 0,
                max: maxValue,
                step: step,
                range: true,
                values: [defaultMinValue, defaultMaxValue],
                slide: function (event, ui) {
                    var values = ui.values;
                    min.text('$' + values[0]);
                    max.text('$' + values[1]);
                }
            });
            var values = el.slider("option", "values");
            min.text('$' + values[0]);
            max.text('$' + values[1]);
        } else {
            // return false;
        }
    }

    function modalInit() {
        var modal = $('.ps-modal');
        if (modal.length) {
            if (modal.hasClass('active')) {
                $('body').css('overflow-y', 'hidden');
            }
        }
        modal.find('.ps-modal__close, .ps-btn--close').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.ps-modal').removeClass('active');
        });
        $('.ps-modal-link').on('click', function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $(target).addClass('active');
            $("body").css('overflow-y', 'hidden');
        });
        $('.ps-modal').on("click", function (event) {
            if (!$(event.target).closest(".ps-modal__container").length) {
                modal.removeClass('active');
                $("body").css('overflow-y', 'auto');
            }
        });
    }

    function searchInit() {
        var searchbox = $('.ps-search');
        $('.ps-search-btn').on('click', function (e) {
            e.preventDefault();
            searchbox.addClass('active');
        });
        searchbox.find('.ps-btn--close').on('click', function (e) {
            e.preventDefault();
            searchbox.removeClass('active');
        });
    }

    function countDown() {
        var time = $(".ps-countdown");
        time.each(function () {
            var el = $(this),
                value = $(this).data('time');
            var countDownDate = new Date(value).getTime();
            var timeout = setInterval(function () {
                var now = new Date().getTime(),
                    distance = countDownDate - now;
                var days = Math.floor(distance / (1000 * 60 * 60 * 24)),
                    hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
                    minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
                    seconds = Math.floor((distance % (1000 * 60)) / 1000);
                el.find('.days').html(days);
                el.find('.hours').html(hours);
                el.find('.minutes').html(minutes);
                el.find('.seconds').html(seconds);
                if (distance < 0) {
                    clearInterval(timeout);
                    el.closest('.ps-section').hide();
                }
            }, 1000);
        });
    }

    function productFilterToggle() {
        $('.ps-filter__trigger').on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            el.find('.ps-filter__icon').toggleClass('active');
            el.closest('.ps-filter').find('.ps-filter__content').slideToggle();
        });
        if ($('.ps-sidebar--home').length > 0) {
            $('.ps-sidebar--home > .ps-sidebar__header > a').on('click', function (e) {
                e.preventDefault();
                $(this).closest('.ps-sidebar--home').children('.ps-sidebar__content').slideToggle();
            })
        }
    }

    /*
    function mainSlider() {
        var homeBanner = $('.ps-carousel--animate');
        homeBanner.slick({
            autoplay: true,
            speed: 1000,
            lazyLoad: 'progressive',
            arrows: false,
            fade: true,
            dots: true,
            prevArrow: "<i class='slider-prev ba-back'></i>",
            nextArrow: "<i class='slider-next ba-next'></i>"
        });
    }
    */

    function subscribePopup() {
        var subscribe = $('#subscribe'),
            time = subscribe.data('time');
        setTimeout(function () {
            if (subscribe.length > 0) {
                subscribe.addClass('active');
                $('body').css('overflow', 'hidden');
            }
        }, time);
        $('.ps-popup__close').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.ps-popup').removeClass('active');
            $('body').css('overflow', 'auto');
        });
        $('#subscribe').on("click", function (event) {
            if (!$(event.target).closest(".ps-popup__content").length) {
                subscribe.removeClass('active');
                $("body").css('overflow-y', 'auto');
            }
        });
    }

    function stickySidebar() {
        var sticky = $('.ps-product--sticky'),
            stickySidebar, checkPoint = 992,
            windowWidth = $(window).innerWidth();
        if (sticky.length > 0) {
            stickySidebar = new StickySidebar('.ps-product__sticky .ps-product__info', {
                topSpacing: 20,
                bottomSpacing: 20,
                containerSelector: '.ps-product__sticky',
            });
            if ($('.sticky-2').length > 0) {
                var stickySidebar2 = new StickySidebar('.ps-product__sticky .sticky-2', {
                    topSpacing: 20,
                    bottomSpacing: 20,
                    containerSelector: '.ps-product__sticky',
                });
            }
            if (checkPoint > windowWidth) {
                stickySidebar.destroy();
                stickySidebar2.destroy();
            }
        } else {
            return false;
        }
    }

    function accordion() {
        var accordion = $('.ps-accordion');
        accordion.find('.ps-accordion__content').hide();
        $('.ps-accordion.active').find('.ps-accordion__content').show();
        accordion.find('.ps-accordion__header').on('click', function (e) {
            e.preventDefault();
            if ($(this).closest('.ps-accordion').hasClass('active')) {
                $(this).closest('.ps-accordion').removeClass('active');
                $(this).closest('.ps-accordion').find('.ps-accordion__content').slideUp(350);

            } else {
                $(this).closest('.ps-accordion').addClass('active');
                $(this).closest('.ps-accordion').find('.ps-accordion__content').slideDown(350);
                $(this).closest('.ps-accordion').siblings('.ps-accordion').find('.ps-accordion__content').slideUp();
            }
            $(this).closest('.ps-accordion').siblings('.ps-accordion').removeClass('active');
            $(this).closest('.ps-accordion').siblings('.ps-accordion').find('.ps-accordion__content').slideUp();
        });
    }

    function progressBar() {
        var progress = $('.ps-progress');
        progress.each(function (e) {
            var value = $(this).data('value');
            $(this).find('span').css({
                width: value + "%"
            })
        });
    }

    function customScrollbar() {
        $('.ps-custom-scrollbar').each(function () {
            var height = $(this).data('height');
            $(this).slimScroll({
                height: height + 'px',
                alwaysVisible: true,
                color: '#000000',
                size: '6px',
                railVisible: true,
            });
        })
    }

    /*
    function select2Cofig() {
        $('select.ps-select').select2({
            placeholder: $(this).data('placeholder'),
            minimumResultsForSearch: -1
        });
    }
    */

    function carouselNavigation() {
        var prevBtn = $('.ps-carousel__prev'),
            nextBtn = $('.ps-carousel__next');
        prevBtn.on('click', function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $(target).trigger('prev.owl.carousel', [1000]);
        });
        nextBtn.on('click', function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $(target).trigger('next.owl.carousel', [1000]);
        });
    }

    /*
    function dateTimePicker() {
        $('.ps-datepicker').datepicker();
    }
    */

    $(function () {
        backgroundImage();
        owlCarouselConfig();
        siteToggleAction();
        subMenuToggle();
        masonry('.ps-masonry');
        productFilterToggle();
        tabs();
        slickConfig();
        // productLightbox();
        rating();
        backToTop();
        stickyHeader();
        filterSlider();
        modalInit();
        searchInit();
        countDown();
        // mainSlider();
        parallax();
        stickySidebar();
        accordion();
        progressBar();
        customScrollbar();
        // select2Cofig();
        carouselNavigation();
        // dateTimePicker();
        $('[data-toggle="tooltip"]').tooltip();
        $('.ps-product--quickview .ps-product__images').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            dots: false,
            arrows: true,
            infinite: false,
            prevArrow: "<a href='#'><i class='fa fa-angle-left'></i></a>",
            nextArrow: "<a href='#'><i class='fa fa-angle-right'></i></a>",
        });
    });
    $('#product-quickview').on('shown.bs.modal', function (e) {
        $('.ps-product--quickview .ps-product__images').slick('setPosition');
    });

    $(window).on('load', function () {
        $('body').addClass('loaded');
        subscribePopup();
    });

    // Live text search|| All shop
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myShop .col-md-3").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

    // Live text search|| Store
    $(document).ready(function () {
        $("#myStoreInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myStore").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

    // Live text search|| Brand
    $(document).ready(function () {
        $("#myBrandInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myBrand .ps-checkbox").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

    // Live text search|| Category
    $(document).ready(function () {
        $("#myCategoryInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myCategory .ps-checkbox").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

    /*division select*/
    $('select.division').on('change', function () {
        var chooseDivision = $(this).children('option:selected').val();

        var url = $('.getDivisionArea').val();
        /*ajax get value*/
        if (url == null) {
            location.reload()
        } else {
            $.ajax({
                url: url,
                method: 'GET',
                data: { id: chooseDivision },
                success: function (result) {
                    $('.area').html(result);
                }
            })
        }
    });


    //get logistics
    $('.area').on('change', function () {
        var division = $('.division').val();
        var area = $(this).val();
        var url = $('.getLogistics').val();
        /*ajax get value*/
        if (url == null) {
            location.reload()
        } else {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url,
                method: 'GET',
                data: { division: division, area: area },
                success: function (result) {
                    $('.logistics').html(result);
                }
            })
        }

    });


})(jQuery);


/*get cart items */
function cartList() {
    var url = $('.cart-list-url').val();
    if (url != null && url != undefined) {
        $.ajax({
            url: url,
            method: 'GET',
            success: function (result) {
                localStorage.removeItem('guest_cart_items');
                let array_cart = [];
                if (result[4] == true) {
                    result[5].forEach((item => {
                        let cart_item = { vProductVS_id: item.vpvs_id, quantity: parseInt(item.quantity), campaign_id: item.campaign_id };
                        array_cart.push(cart_item);
                        localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
                    }))
                } else {
                    result[5].forEach((item => {
                        let cart_item = { vProductVS_id: item.product_stock_id, quantity: parseInt(item.quantity), campaign_id: item.campaign_id };
                        array_cart.push(cart_item);
                        localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
                    }))
                }
                guestCartList();
            },
            error: function () {
                localStorage.removeItem('guest_cart_items');
                guestCartList();
            }
        })
    }
}


function addToCart(id, campaign_id = null) {
    //db cart
    var url = $('.add-to-cart-url').val();
    if (campaign_id == null) {
        var cart_quantity = $('.cart-quantity').val();
    } else {
        var cart_quantity = $('.cart-quantity-' + id).val();
    }
    if (id != null && id != "" && url != null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'POST',
            data: { vProductVS_id: id, quantity: parseInt(cart_quantity), campaign_id: campaign_id },
            success: function (result) {
                if (localStorage.getItem('guest_cart_items') === null) {
                    cartList();
                }
            }
        })
    }

    //local cart
    if (id != null && id != "") {
        let old_cart_items = JSON.parse(localStorage.getItem('guest_cart_items'));
        let array_cart = [];
        let cart_item = { vProductVS_id: id, quantity: parseInt(cart_quantity), campaign_id: campaign_id };

        if (old_cart_items != null) {
            old_cart_items.forEach(old_cart_item => {
                array_cart.push(old_cart_item);
            });
            if (array_cart.includes(array_cart.find(guest_cart_item => guest_cart_item.vProductVS_id === id))) {
                let temp_item = array_cart.find(guest_cart_item => guest_cart_item.vProductVS_id === id);
                let temp_old_cart_items = old_cart_items.filter(old_cart_item => {
                    return old_cart_item.vProductVS_id !== id;
                });
                temp_old_cart_items.push({ vProductVS_id: id, quantity: parseInt(temp_item.quantity) + 1, campaign_id: campaign_id });
                localStorage.setItem('guest_cart_items', JSON.stringify(temp_old_cart_items));
                toastr.info('Quantity has been increased', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
            else {
                array_cart.push(cart_item);
                localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
                toastr.success('Added to cart', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
        } else {
            array_cart.push(cart_item);
            localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
            toastr.success('Added to cart', toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "30000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        }
        guestCartList();
    }
}

function increaseGuestCart(id) {
    let old_cart_items = JSON.parse(localStorage.getItem('guest_cart_items'));
    let array_cart = [];
    if (old_cart_items != null) {
        old_cart_items.forEach((old_cart_item, index) => {
            if (index + 1 !== id) {
                array_cart.push(old_cart_item);
            }
            else {
                let temp_item = { vProductVS_id: old_cart_item.vProductVS_id, quantity: parseInt(old_cart_item.quantity) + 1, campaign_id: old_cart_item.campaign_id }
                array_cart.push(temp_item);
            }
        });
        localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
        toastr.info('Quantity has been increased', toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
        guestCartList();
    }
}


function decreaseGuestCart(id) {
    let old_cart_items = JSON.parse(localStorage.getItem('guest_cart_items'));
    let array_cart = [];
    if (old_cart_items != null) {
        old_cart_items.forEach((old_cart_item, index) => {
            if (index + 1 !== id) {
                array_cart.push(old_cart_item);
            }
            else {
                let temp_item = { vProductVS_id: old_cart_item.vProductVS_id, quantity: old_cart_item.quantity > 1 ? parseInt(old_cart_item.quantity) - 1 : 1, campaign_id: old_cart_item.campaign_id }
                array_cart.push(temp_item);
            }
        });
        localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
        toastr.error('Quantity has been decreased', toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
        guestCartList();
    }
}


/*get cart items */
function guestCartList() {
    var url = $('.guest-cart-list-url').val();
    var empty = $('.cart-empty').val();
    var app_url = $('.app_url').val();
    var is_active_guest_checkout = $('.is-active-guest-checkout').val();

    if (url != null && url != undefined && localStorage.getItem('guest_cart_items') !== null) {
        $.ajax({
            url: url,
            method: 'GET',
            data: {
                carts: JSON.parse(localStorage.getItem('guest_cart_items')),
            },
            success: function (result) {
                //navbar no. of carts
                $('.total_update_price').empty();
                $('.total_update_tax').empty();
                $('.total_update_total').empty();

                $('.total_update_price').append(result[1]);
                $('.total_update_tax').append(result[2]);
                $('.total_update_total').append(result[3]);

                $(".navbar-cart").empty();
                if (result[0].length == 0) {
                    $('.navbar-cart').text(0);
                } else {
                    $('.navbar-cart').text(result[0].length)
                }

                //show item in cart dropdown
                var html = "";
                var blade_html = "";

                var footer = "";
                if (result[4] == true) {
                    if (result[0].length > 0) {
                        result[0].forEach(function (item, index) {
                            $('.show-cart-items').empty();
                            $('.guest-cart-blade').empty();
                            html += '<div class="ps-product--cart-mobile">' +
                                '                               <div class="ps-product__thumbnail mt-2"><a href="' + item.url + '"><img src="' + item.img + '" alt=""></a></div>' +
                                '                               <div class="ps-product__content">' +
                                '                                   <a class="ps-product__remove" href="#!"><i class="icon-cross" onclick="deleteGuestCart(' + item.id + ')"></i></a><a href="' + item.url + '">' + item.name + '</a>' +
                                '                                  <p><strong>Sold by: </strong>' + item.shop_name + '</p>' +
                                '                                   <p><small>' + item.quantity + ' x ' + item.price + '</small></p>' +
                                '                               </div>' +
                                '                            </div>';

                            blade_html += '<tr>' +
                                '                                      <td>' +
                                '                                      <div class="ps-product--cart">' +
                                '                                          <div class="ps-product__thumbnail"><a target="_blank" href="' + item.url + '">' +
                                '                                              <img src="' + item.img + '" alt="">' +
                                '                                            </a></div>' +
                                '                                          <div class="ps-product__content">' +
                                '                                           <a href="' + item.url + '">' + item.name + '</a>' +
                                '                                           <p>Sold By:<strong>' + item.shop_name + '</strong></p>' +
                                '                                          </div>' +
                                '                                      </div>' +
                                '                                  </td>' +
                                '                                  <td class="price">' +
                                '                                       ' + item.price + ' ' +
                                '                                    </td>' +
                                '                                  <td>' +
                                '                                      <div class="form-group--number">' +
                                '                                        <span class="input-number-decrement"' +
                                '                                        onclick="decreaseGuestCart(' + item.id + ')"' +
                                '                                        data-id="{{ $cart->id }}"' +
                                '                                        >–</span>' +
                                '                                        <input id="input-number-{{ $cart->id }}" class="input-number quantity_update"' +
                                '                                        data-url="{{ route(\'update.to.cart\') }}"' +
                                '                                        type="text"' +
                                '                                        name="quantity_update"' +
                                '                                        min="1"' +
                                '                                        max="{{ $cart->stock }}"' +
                                '                                        value="' + item.quantity + '" readonly>' +
                                '                                        <span class="input-number-increment"' +
                                '                                        onclick="increaseGuestCart(' + item.id + ')"' +
                                '                                        data-id="{{ $cart->id }}"' +
                                '                                        >+</span>' +
                                '                                      </div>' +
                                '                                  </td>' +
                                '                                  <td class="updated_price-{{ $cart->id }}">' + item.blade_quantity_x_price + '</td>' +
                                '                                  <td><a href="javascript:void()" onclick="deleteGuestCart(' + item.id + ')"><i class="icon-cross"></i></a></td>' +
                                '                                  <tr>';
                        });
                        if (result[5] == true) {
                            $('.cart-items-footer').empty();
                            footer += '<h3>Total:<strong>' + result[1] + '</strong></h3>' +
                                '<figure><a class="ps-btn" href="' + app_url + '/shopping-cart">View Cart</a><a class="ps-btn" href="' + app_url + '/checkout">Checkout</a></figure>'
                        }
                        else {
                            $('.cart-items-footer').empty();
                            if (is_active_guest_checkout == 'YES') {
                                footer += '<h3>Total:<strong>' + result[1] + '</strong></h3>' +
                                    '<figure><a class="ps-btn guest-view-cart" href="' + app_url + '/guest/shopping-cart">View Cart</a><a class="ps-btn" href="' + app_url + '/guest/checkout">Guest Checkout</a></figure>'
                            }
                            else {
                                footer += '<h3>Total:<strong>' + result[1] + '</strong></h3>' +
                                    '<figure><a class="ps-btn" href="' + app_url + '/guest/shopping-cart">View Cart</a><a class="ps-btn" href="' + app_url + '/checkout">Checkout</a></figure>'
                            }
                        }
                    } else {
                        $('.show-cart-items').empty();
                        $('.guest-cart-blade').empty();
                        $('.cart-items-footer').empty();
                        html += '<div class="ps-product--cart-mobile">' +
                            '<img src="' + empty + '" class="img-fluid" alt="empty cart"/>' +
                            '</div>';
                        footer += '<h3 class="text-center">No item in your cart</h3>'
                    }
                } else {
                    if (result[0].length > 0) {
                        result[0].forEach(function (item, index) {
                            $('.show-cart-items').empty();
                            $('.guest-cart-blade').empty();
                            html += '<div class="ps-product--cart-mobile">' +
                                '                               <div class="ps-product__thumbnail mt-2"><a href="' + item.url + '"><img src="' + item.img + '" alt=""></a></div>' +
                                '                               <div class="ps-product__content">' +
                                '                                   <a class="ps-product__remove" href="#!"><i class="icon-cross" onclick="deleteGuestCart(' + item.id + ')"></i></a><a href="' + item.url + '">' + item.name + '</a>' +
                                '                                  <p><small>' + item.quantity + ' x ' + item.price + '</small></p>' +
                                '                               </div>' +
                                '                            </div>';
                            blade_html += '<tr>' +
                                '                                      <td>' +
                                '                                      <div class="ps-product--cart">' +
                                '                                          <div class="ps-product__thumbnail"><a target="_blank" href="' + item.url + '">' +
                                '                                              <img src="' + item.img + '" alt="">' +
                                '                                            </a></div>' +
                                '                                          <div class="ps-product__content">' +
                                '                                           <a href="' + item.url + '">' + item.name + '</a>' +
                                '                                          </div>' +
                                '                                      </div>' +
                                '                                  </td>' +
                                '                                  <td class="price">' +
                                '                                       ' + item.price + ' ' +
                                '                                    </td>' +
                                '                                  <td>' +
                                '                                      <div class="form-group--number">' +
                                '                                        <span class="input-number-decrement"' +
                                '                                        onclick="decreaseGuestCart(' + item.id + ')"' +
                                '                                        data-id="{{ $cart->id }}"' +
                                '                                        >–</span>' +
                                '                                        <input id="input-number-{{ $cart->id }}" class="input-number quantity_update"' +
                                '                                        data-url="{{ route(\'update.to.cart\') }}"' +
                                '                                        type="text"' +
                                '                                        name="quantity_update"' +
                                '                                        min="1"' +
                                '                                        max="{{ $cart->stock }}"' +
                                '                                        value="' + item.quantity + '" readonly>' +
                                '                                        <span class="input-number-increment"' +
                                '                                        onclick="increaseGuestCart(' + item.id + ')"' +
                                '                                        data-id="{{ $cart->id }}"' +
                                '                                        >+</span>' +
                                '                                      </div>' +
                                '                                  </td>' +
                                '                                  <td class="updated_price-{{ $cart->id }}">' + item.blade_quantity_x_price + '</td>' +
                                '                                  <td><a href="javascript:void()" onclick="deleteGuestCart(' + item.id + ')"><i class="icon-cross"></i></a></td>' +
                                '                                  <tr>';
                        });

                        if (result[5] == true) {
                            $('.cart-items-footer').empty();
                            footer += '<h3>Total:<strong>' + result[1] + '</strong></h3>' +
                                '<figure><a class="ps-btn" href="' + app_url + '/shopping-cart">View Cart</a><a class="ps-btn" href="' + app_url + '/checkout">Checkout</a></figure>'
                        }
                        else {
                            $('.cart-items-footer').empty();
                            if (is_active_guest_checkout == 'YES') {
                                footer += '<h3>Total:<strong>' + result[1] + '</strong></h3>' +
                                    '<figure><a class="ps-btn guest-view-cart" href="' + app_url + '/guest/shopping-cart">View Cart</a><a class="ps-btn" href="' + app_url + '/guest/checkout">Guest Checkout</a></figure>'
                            }
                            else {
                                footer += '<h3>Total:<strong>' + result[1] + '</strong></h3>' +
                                    '<figure><a class="ps-btn" href="' + app_url + '/guest/shopping-cart">View Cart</a><a class="ps-btn" href="' + app_url + '/checkout">Checkout</a></figure>'
                            }
                        }
                    } else {
                        $('.show-cart-items').empty();
                        html += '<div class="ps-product--cart-mobile">' +
                            '<img src="' + empty + '" class="img-fluid" alt="empty cart"/>' +
                            '</div>';

                        $('.cart-items-footer').empty();
                        footer += '<h3 class="text-center">No item in your cart</h3>'
                    }
                }
                $(".show-cart-items").append(html)
                $(".guest-cart-blade").append(blade_html)
                $(".cart-items-footer").append(footer)
            },
            error: function () {
                localStorage.removeItem('guest_cart_items');
                guestCartList();
            }
        })
    }
    else {
        //navbar no. of carts
        $('.total_update_price').empty();
        $('.total_update_tax').empty();
        $('.total_update_total').empty();

        $('.total_update_price').text('$0.00');
        $('.total_update_tax').text('$0.00');
        $('.total_update_total').text('$0.00');
        $(".navbar-cart").empty();
        $('.navbar-cart').text(0);

        //show item in cart dropdown
        var html = "";
        var blade_html = "";
        var footer = "";
        html += '<div class="ps-product--cart-mobile">' +
            '<img src="' + empty + '" class="img-fluid" alt="empty cart"/>' +
            '</div>';
        footer += '<h3 class="text-center">No item in your cart</h3>';

        $('.show-cart-items').empty();
        $('.guest-cart-blade').empty();
        $('.cart-items-footer').empty();

        $(".show-cart-items").append(html);
        $('.guest-cart-blade').append(blade_html);
        $(".cart-items-footer").append(footer);
    }
}


//guest add to cart
function addToGuestCart(id, campaign_id = null) {
    if (campaign_id == null) {
        var cart_quantity = $('.cart-quantity').val();
    } else {
        var cart_quantity = $('.cart-quantity-' + id).val();
    }
    if (id != null && id != "") {
        let old_cart_items = JSON.parse(localStorage.getItem('guest_cart_items'));
        let array_cart = [];
        let cart_item = { vProductVS_id: id, quantity: parseInt(cart_quantity), campaign_id: campaign_id };

        if (old_cart_items != null) {
            old_cart_items.forEach(old_cart_item => {
                array_cart.push(old_cart_item);
            });
            if (array_cart.includes(array_cart.find(guest_cart_item => guest_cart_item.vProductVS_id === id))) {

                let temp_item = array_cart.find(guest_cart_item => guest_cart_item.vProductVS_id === id);
                let temp_old_cart_items = old_cart_items.filter(old_cart_item => {
                    return old_cart_item.vProductVS_id !== id;
                });
                temp_old_cart_items.push({ vProductVS_id: id, quantity: parseInt(temp_item.quantity) + 1, campaign_id: campaign_id });
                //delete old cart items--
                localStorage.removeItem('guest_cart_items');
                localStorage.setItem('guest_cart_items', JSON.stringify(temp_old_cart_items));
                toastr.info('Quantity has been increased', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
            else {
                array_cart.push(cart_item);
                //remove old carts--
                localStorage.removeItem('guest_cart_items');
                localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
                toastr.success('Added to cart', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
        } else {
            array_cart.push(cart_item);
            localStorage.setItem('guest_cart_items', JSON.stringify(array_cart));
            toastr.success('Added to cart', toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "30000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        }
    }
    guestCartList();
}
//end guest add to cart


/**
 * Cart Update
 */

// Decrement
function quantityUpdateDec(ele) {

    var url = $('.quantity_update').attr('data-url');
    var id = $(ele).attr('data-id');

    if (id != null && id != "" && url != null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'GET',
            data: {
                idDec: id,
            },
            success: function (result) {
                //notification
                if (result.error != null) {
                    toastr.warning(result.error, toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
                } else {
                    $('#input-number-' + result.quantity.id).val(result.quantity.quantity);
                    $('.updated_price-' + result.quantity.id).html(result.updated_price);

                    toastr.success(result.message, toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
                }
                if (localStorage.getItem('guest_cart_items') === null) {
                    cartList();
                }
            }
        })
    } else {
        location.reload()
    }
}

// Increment
function quantityUpdateInc(ele) {
    var url = $('.quantity_update').attr('data-url');
    var id = $(ele).attr('data-id');
    var dataVal = $(ele).val();

    if (id != null && id != "" && url != null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'GET',
            data: {
                idInc: id,
                quantity: parseInt(dataVal),
            },
            success: function (result) {
                //notification
                if (result.error != null) {
                    toastr.warning(result.error, toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
                } else {
                    $('#input-number-' + result.quantity.id).val(result.quantity.quantity);
                    $('.updated_price-' + result.quantity.id).html(result.updated_price);
                    toastr.success(result.message, toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
                }
                if (localStorage.getItem('guest_cart_items') === null) {
                    cartList();
                }
            }
        })
    } else {
        location.reload()
    }
}


//delete cart item
function deleteCart(cart_id) {
    var url = $('.remove-from-cart-url').val();
    if (cart_id != null && cart_id != "" && url != null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'POST',
            data: { id: cart_id },
            success: function (result) {
                $('#shopping_cart-' + cart_id).remove();
                cartList();
                //notification
                toastr.error(result.message, toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
        })
    }
}



//delete guest cart item
function deleteGuestCart(cart_id) {
    var guest_url = $('.guest-remove-from-cart-url').val();
    if (cart_id != null && cart_id != "" && guest_url != null) {
        let old_cart_items = JSON.parse(localStorage.getItem('guest_cart_items'));
        let delete_item = old_cart_items.filter((old_cart_item, index) => {
            return index + 1 === cart_id;
        });
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: guest_url,
            method: 'POST',
            data: { delete_item: delete_item },
            success: function () {
                let old_cart_items = JSON.parse(localStorage.getItem('guest_cart_items'));
                let temp_old_cart_items = old_cart_items.filter((old_cart_item, index) => {
                    return index + 1 !== cart_id;
                });
                if (temp_old_cart_items.length > 0) {
                    localStorage.setItem('guest_cart_items', JSON.stringify(temp_old_cart_items));
                } else {
                    localStorage.removeItem('guest_cart_items');
                    cartList();
                }
                guestCartList();

                //notification
                toastr.error('Item has been removed', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
        })
    }
}


/*get wishlist items */
function wishList() {
    var url = $('.wishlist-url').val();
    var empty = $('.wishlist-empty').val();
    var app_url = $('.app_url').val();

    if (url != null && url != undefined) {
        $.ajax({
            url: url,
            method: 'GET',
            success: function (result) {
                //navbar no. of wishlist
                $(".navbar-wishlist").empty();
                if (result.length == 0) {
                    $('.navbar-wishlist').text(0);
                } else {
                    $('.navbar-wishlist').text(result.length);
                }

                //show item in cart dropdown
                var html = "";
                var footer = "";
                if (result.length > 0) {
                    result.forEach(function (item, index) {
                        $('.show-wishlist-items').empty();
                        $('.wishlist-items-footer').empty();
                        html += '<div class="ps-product--cart-mobile">' +
                            '                               <div class="ps-product__thumbnail mt-2"><a href="' + app_url + '/product/' + item.sku + '/' + item.slug + '"><img src="' + item.image + '" alt=""></a></div>' +
                            '                               <div class="ps-product__content">' +
                            '                                   <a class="ps-product__remove" href="#!"><i class="icon-cross" onclick="deleteWishlist(' + item.id + ')"></i></a><a href="' + app_url + '/product/' + item.sku + '/' + item.slug + '">' + item.name + '</a>' +
                            '                                  <p><strong>Price Range: </strong><span class="text-danger">' + item.range + '</span></p>' +
                            '                               </div>' +
                            '                            </div>';
                    })
                    footer += '<figure class="d-flex justify-content-center"><a class="ps-btn" href="' + app_url + '/wishlist">View Wishlist</a></figure>'
                } else {
                    $('.show-wishlist-items').empty();
                    $('.wishlist-items-footer').empty();
                    html += '<div class="ps-product--cart-mobile">' +
                        '<img src="' + empty + '" class="img-fluid" alt="empty cart"/>' +
                        '</div>';
                    footer += '<h3 class="text-center">No item in your wishlist</h3>'
                }

                $(".show-wishlist-items").append(html)
                $(".wishlist-items-footer").append(footer)
            }
        })
    }
}

//Add to wishlist
function addToWishlist(product_id) {

    try {
        event.preventDefault();
    } catch (e) {

    }
    var url = $('.add-to-wishlist-url').val();

    if (product_id != null && product_id != "" && url != null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'POST',
            data: { product_id: product_id },
            success: function (result) {
                //notification
                toastr.success(result.message, toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
                var is_auth = $('.auth-check').val();
                if (is_auth == 1) {
                    wishList();
                }
            }
        })
    }
}

//Go To Auth
function goToAuth() {
    event.preventDefault();
    var goAuth = $('.go-to-auth').val();
    window.location = goAuth;
}

//delete wishlist item
function deleteWishlist(wishlist_id) {
    var url = $('.remove-from-wishlist-url').val();
    if (wishlist_id != null && wishlist_id != null && url != null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'POST',
            data: { id: wishlist_id },
            success: function (result) {
                //notification
                toastr.error(result.message, toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
                var is_auth = $('.auth-check').val();
                if (is_auth == 1) {
                    wishList();
                }
            }
        });

        localStorage.removeItem('wish_list');
    } else {
        location.reload()
    }
}

/*get compare items */
function compareList() {
    var url = $('.compare-list-url').val();
    var empty = $('.compare-empty').val();
    var app_url = $('.app_url').val();

    var product_items = JSON.parse(localStorage.getItem('compare_products'));
    var product_items_for_web = localStorage.getItem('compare_products');
    if (url != null && url != undefined) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: 'POST',
            data: { products: product_items },
            success: function (result) {
                //navbar no. of comparison
                $(".navbar-comparison").empty();
                if (result.length == 0) {
                    $('.navbar-comparison').text(0);
                } else {
                    $('.navbar-comparison').text(result.length);
                }
                //show item in cart dropdown
                var html = "";
                var footer = "";
                if (result.length > 0) {
                    result.forEach(function (item, index) {
                        $('.show-comparison-items').empty();
                        $('.comparison-items-footer').empty();
                        html += '<div class="ps-product--cart-mobile">' +
                            '                               <div class="ps-product__thumbnail mt-2"><a href="' + app_url + '/product/' + item.sku + '/' + item.slug + '"><img src="' + app_url + '/public/' + item.image + '" alt=""></a></div>' +
                            '                               <div class="ps-product__content">' +
                            '                                   <a class="ps-product__remove" href="#!"><i class="icon-cross" onclick="deleteCompare(' + item.id + ')"></i></a><a href="' + app_url + '/product/' + item.sku + '/' + item.slug + '">' + item.name + '</a>' +
                            '                                  <p><strong>Price Range: </strong><span class="text-danger">' + item.range + '</span></p>' +
                            '                               </div>' +
                            '                            </div>';
                    })
                    footer += '<figure class="d-flex justify-content-center">' +
                        '<form action="' + app_url + '/comparison">' +
                        '<input type="hidden" name="products[]" value="' + product_items_for_web + '">' +
                        '<button class="ps-btn" type="submit">Compare</button>' +
                        '</form>' +
                        '</figure>'
                } else {
                    $('.show-comparison-items').empty();
                    $('.comparison-items-footer').empty();
                    html += '<div class="ps-product--cart-mobile">' +
                        '<img src="' + empty + '" class="img-fluid" alt="empty caomparison"/>' +
                        '</div>';
                    footer += '<h3 class="text-center">No item for comparison</h3>'
                }

                $(".show-comparison-items").append(html)
                $(".comparison-items-footer").append(footer)
            }
        })
    }
}

//Add to compare
function addToCompare(product_id) {
    event.preventDefault();
    if (product_id != null) {
        let old_items = JSON.parse(localStorage.getItem('compare_products'));
        if (old_items != null) {
            if (old_items.length > 2) {
                toastr.error('You have reached maximum number of products to compare at once.', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            } else {
                let array_compare = [];
                old_items.forEach(old_item => {
                    array_compare.push(old_item);
                });
                if (array_compare.includes(product_id)) {
                    toastr.error('Item already exist in comparison', toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
                } else {
                    array_compare.push(product_id);
                    localStorage.removeItem('compare_products');
                    localStorage.setItem('compare_products', JSON.stringify(array_compare));
                    toastr.success('Added for comparison', toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-center",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "30000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    });
                }
            }
        } else {
            let array_compare = [product_id];
            localStorage.setItem('compare_products', JSON.stringify(array_compare));
            toastr.success('Added for comparison', toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "30000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            });
        }
        compareList();
    } else {
        location.reload()
    }
}

//delete compare item
function deleteCompare(product_id) {
    var app_url = $('.app_url').val();
    let old_items = JSON.parse(localStorage.getItem('compare_products'));
    if (product_id != null) {
        let array_compare = [];
        old_items.forEach(old_item => {
            if (old_item != product_id) {
                array_compare.push(old_item);
            }
        });
        localStorage.removeItem('compare_products');
        localStorage.setItem('compare_products', JSON.stringify(array_compare));
        compareList();
        toastr.error('Item removed from comparison', toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });

    } else {
        location.reload()
    }
}

// myLogistic
function myLogistic(ele) {
    var url = $('.logistic_url').val();
    var dataID = $(ele).attr('data-id');
    var logisticId = $(ele).attr('data-logisticId');
    var shipping_id = '#' + dataID;
    var shipping_amount = $(shipping_id).val();
    var amount = $('.amount').val();
    var forLogisticsAmount = $('.forLogisticsAmount').val();

    $('.get_logistic_id').val(logisticId);
    $('.get_shipping_value').val(shipping_amount);

    /*ajax get value*/
    if (url == null) {
        location.reload()
    } else {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: url,
            method: 'GET',
            data: {
                shipping_amount: shipping_amount,
                amount: amount,
                forLogisticsAmount: forLogisticsAmount
            },
            success: function (result) {

                $('.newTotal').html(result.data);
                $('.newTotalHidden').val(result.data);
                $('.newTotalWithOutFormat').val(result.data2);

                toastr.success('Amount calculated', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
            }
        })
    }

}


// myLogistic
function getVendor(ele) {
    var url = $('.color_url').val();
    var variant_id = $(ele).attr('data-id');
    var product_id = $(ele).attr('data-product');

    /*ajax get value*/
    if (url == null) {
        location.reload()
    } else {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: url,
            method: 'GET',
            data: { variant_id: variant_id, product_id: product_id },
            success: function (result) {
                $('.newTotal').html(result.data);

                toastr.success(result.message, toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-left",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });

            }
        })
    }

}

// Prince
//show the modal in this function
function quickView(url) {
    $('#product-quickview').modal('show');
    $('#product-quickview').load(url);
}


function forModal(url, message) {
    event.preventDefault();
    $("#show-modal").modal("show");
    $("#title").text(message);
    $("#show-form").load(url);
    $("body").on("shown.bs.modal", ".modal", function () {
        $(this)
            .find("select")
            .each(function () {
                var dropdownParent = $(document.body);
                if ($(this).parents(".modal.in:first").length !== 0)
                    dropdownParent = $(this).parents(".modal.in:first");
                /*
                $(this).select2({
                    dropdownParent: dropdownParent,
                    templateResult: formatState,
                    templateSelection: formatState,
                });
                */
            });
    });
}

/**
 * Smooth scroll
 */
$(document).ready(function () {
    // Add smooth scrolling to all links
    $("#check_shop").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});


/**
 * Sub Total
 */
$(document).ready(function () {
    if (localStorage.getItem('guest_cart_items') === null) {
        cartList();
    }
    var is_auth = $('.auth-check').val();
    if (is_auth == 1) {
        wishList();
    }
    compareList();
    var sub_total = $('#sub_total').val();
    $('#coupon_sub_total').val(sub_total);
});

/**
 * Track Order
 */
function trackOrder(e) {

    $("#trackForm").on('submit', function () {
        e.preventDefault();
    });

    var url = $('#url').val();
    var order_number = $('#order_number').val();
    var email = $('#email').val();


    /**
     * Ajax Request Header setup
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Submit form data using ajax
     */

    $('#loading').html('Tracking Order....');

    $.ajax({
        url: url,
        method: 'GET',
        data: {
            order_number: order_number,
            email: email
        },
        success: function (response) {
            console.clear();
            $('#loading').addClass('d-none');
            $('#noResult').empty();
            $('#trackResult').empty();
            if (response.logistic != null) {

                var status = response.status;
                // confirmed
                if (status == 'confirmed') {
                    var confirmed = 'completed';
                }
                // processing
                if (status == 'processing') {
                    var processing = 'completed';
                }
                // quality_check
                if (status == 'quality_check') {
                    var quality_check = 'completed';
                }
                // product_dispatched
                if (status == 'product_dispatched') {
                    var product_dispatched = 'completed';
                }
                // delivered
                if (status == 'delivered') {
                    var delivered = 'completed';
                }

                $('#trackResult').html(
                    '<div class="container padding-bottom-3x mb-1">\n' +
                    '        <div class="card mb-3">\n' +
                    '          <div class="p-4 text-center text-white text-lg bg-dark rounded-top"><span class="text-uppercase">Tracking Order No - </span><span class="text-medium">' + '#' + response.booking_code + '</span></div>\n' +
                    '          <div class="d-flex flex-wrap flex-sm-nowrap justify-content-between py-3 px-2 bg-secondary">\n' +
                    '            <div class="w-100 text-center py-1 px-2"><span class="text-medium">Shipped Via:</span> ' + response.logistic.name + ' </div>\n' +
                    '            <div class="w-100 text-center py-1 px-2"><span class="text-medium">Status:</span> ' + response.status + ' </div>\n' +
                    '          </div>\n' +
                    '          <div class="card-body">\n' +
                    '            <div class="steps d-flex flex-wrap flex-sm-nowrap justify-content-between padding-top-2x padding-bottom-1x">\n' +
                    '              <div class="step ' + confirmed + ' ' + processing + ' ' + quality_check + ' ' + product_dispatched + ' ' + delivered + '">\n' +
                    '                <div class="step-icon-wrap">\n' +
                    '                  <div class="step-icon"><i class="icon-check"></i></div>\n' +
                    '                </div>\n' +
                    '                <h4 class="step-title">Confirmed Order</h4>\n' +
                    '              </div>\n' +
                    '              <div class="step ' + processing + ' ' + quality_check + ' ' + product_dispatched + ' ' + delivered + ' ">\n' +
                    '                <div class="step-icon-wrap">\n' +
                    '                  <div class="step-icon"><i class="icon-ticket"></i></div>\n' +
                    '                </div>\n' +
                    '                <h4 class="step-title">Processing Order</h4>\n' +
                    '              </div>\n' +
                    '              <div class="step ' + quality_check + ' ' + product_dispatched + ' ' + delivered + ' ">\n' +
                    '                <div class="step-icon-wrap">\n' +
                    '                  <div class="step-icon"><i class="icon-thumbs-up"></i></div>\n' +
                    '                </div>\n' +
                    '                <h4 class="step-title">Quality Checked</h4>\n' +
                    '              </div>\n' +
                    '              <div class="step ' + product_dispatched + ' ' + delivered + ' ">\n' +
                    '                <div class="step-icon-wrap">\n' +
                    '                  <div class="step-icon"><i class="icon-truck"></i></div>\n' +
                    '                </div>\n' +
                    '                <h4 class="step-title">Product Dispatched</h4>\n' +
                    '              </div>\n' +
                    '              <div class="step ' + delivered + ' ">\n' +
                    '                <div class="step-icon-wrap">\n' +
                    '                  <div class="step-icon"><i class="icon-gift"></i></div>\n' +
                    '                </div>\n' +
                    '                <h4 class="step-title">Product Delivered</h4>\n' +
                    '              </div>\n' +
                    '            </div>\n' +
                    '          </div>\n' +
                    '        </div>'
                );
            } else {
                $('#noResult').html('<h3 class="text-center">No Order Found</h3>');
            }


        }
    });
}

// Self-executing function
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


/**
 * FILTER
 */

$(document).ready(function () {
    $('#sort_filter').on('change', function () {
        $('#sort_form').submit();
    });

});


/**SEARCH FILTER */
$(document).ready(function () {

    $('#filter_input').on('keyup', function () {
        var url = $('#filter_url').val();
        var type = $('#filter_type').val();
        var input = $('#filter_input').val();

        /*ajax get value*/
        if (url === null) {
            location.reload()
        } else {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url,
                method: 'GET',
                data: {
                    type: type,
                    input: input
                },
                success: function (result) {
                    if (input === null || input === '') {
                        $('#show_data').addClass('d-none');
                        $('.search-table').addClass('d-none');
                    } else {
                        $('#show_data').html(result);
                        $('#show_data').removeClass('d-none');
                        $('.search-table').removeClass('d-none');
                    }
                }
            });


        }
    })
});


/** Mobile SEARCH FILTER */
$(document).ready(function () {

    $('#mobile_filter_input').on('keyup', function () {
        var url = $('#mobile_filter_url').val();
        var type = $('#mobile_filter_type').val();
        var input = $('#mobile_filter_input').val();

        /*ajax get value*/
        if (url === null) {
            location.reload()
        } else {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url,
                method: 'GET',
                data: {
                    type: type,
                    input: input
                },
                success: function (result) {
                    if (input === null || input === '') {
                        $('#mobile_show_data').addClass('d-none');
                    } else {
                        $('#mobile_show_data').html(result);
                        $('#mobile_show_data').removeClass('d-none');
                    }
                }
            });


        }
    })
});


// increament decreament
function increaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 1 : value;
    value++;
    document.getElementById('number').value = value;
}

function decreaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 1 : value;
    value < 1 ? value = 1 : '';
    value--;
    if (value <= 0) {
        value = 1;
    }
    document.getElementById('number').value = value;
}

// increment decrement
function increaseValueCamp(ele) {
    var increaseMax = $('#number' + ele.id).attr('data-max');
    var id_value = $('#number' + ele.id).val();
    var valueInc = parseInt(id_value, increaseMax);
    valueInc = isNaN(valueInc) ? 1 : valueInc;
    if (increaseMax <= valueInc) {
        valueInc == increaseMax;
    }
    else {
        valueInc++;
    }
    document.getElementById('number' + ele.id).value = valueInc;
}

function decreaseValueCamp(ele) {
    var value = parseInt(document.getElementById('number' + ele.id).value, 10);
    value = isNaN(value) ? 1 : value;
    value < 1 ? value = 1 : value;
    value--;
    if (value <= 0) {
        value = 1;
    }
    document.getElementById('number' + ele.id).value = value;
}

/**
 * COOKIES FOR MODAL
 */

window.setTimeout(function () {
    // First check, if localStorage is supported.
    if (window.localStorage) {
        // Get the expiration date of the previous popup.
        var nextPopup = localStorage.getItem('subscribeModal-lg');

        if (nextPopup > new Date()) {
            return;
        }

        // Store the expiration date of the current popup in localStorage.
        var expires = new Date();
        expires = expires.setHours(expires.getHours() + 24);

        localStorage.setItem('subscribeModal-lg', expires);
    }

    $('.subscribeModal-lg').modal('show');
}, 1000);


$(document).ready(function () {
    setInterval(function () {
        localStorage.removeItem('subscribeModal-lg');
    }, 3600000);
});


var TxtType = function (el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function () {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) {
        delta /= 2;
    }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function () {
        that.tick();
    }, delta);
};





function pushToStorage(arr) {
    if (typeof (Storage) !== "undefined") {

        localStorage.setItem("wish_list", JSON.stringify(arr));
    } else {
        console.log("your browser does not support Storage");
    }
}


function pushToStorageWishID(arr) {
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem("wish_id", JSON.stringify(arr));
    } else {
        console.log("your browser does not support Storage");
    }
}

function loadExisting() {
    var stored_wishList = JSON.parse(localStorage.getItem("wish_list"));
    
    if (typeof stored_wishList != 'undefined' && stored_wishList != null) {
        wish_list = stored_wishList;
        $("#show-wishlist-items").empty();
        $(".show-all-wishlist").empty();
        stored_wishList.forEach(function (item, index, arr) {
            $("#show-wishlist-items").append(item);
            $(".show-all-wishlist").append(item);
        });
        count_items_in_wishlist_update();
    }
}

$(document).ready(function () {
    loadExisting();
    count_items_in_wishlist_update();
    $(".wishlist").on("click", function (e) {
        e.preventDefault()
        var data = "";
        var product_id = $(this).attr("data-product_id");
        var product_name = $(this).attr("data-product_name");
        var product_price = $(this).attr("data-product_price");
        var product_sku = $(this).attr("data-product_sku");
        var product_slug = $(this).attr("data-product_slug");
        var product_image = $(this).attr("data-product_image");
        var app_url = $(this).attr("data-app_url");
        //check if the element is in the array
        if ($.inArray(product_id, wish_list) == -1) {
            if (productId.indexOf(product_id) < 0) {
                var last_id = wish_list.length == 0 ? 0 : wish_list.length;

                var product_str = '<div class="ps-product--cart-mobile p-' + product_id + '" id="list_id_' + last_id + '">' +
                    '<input type="hidden" class="list_id_' + last_id + '" value="' + product_id + '">' +
                    '                               <div class="ps-product__thumbnail mt-2"><a href="' + app_url + '/product/' + product_sku + '/' + product_slug + '"><img src="' + product_image + '" alt=""></a></div>' +
                    '                               <div class="ps-product__content">' +
                    '                                   <a class="ps-product__remove w-premove" wpid="' + last_id + '" href="#!"><i class="icon-cross"></i></a><a href="' + app_url + '/product/' + product_sku + '/' + product_slug + '">' + product_name + '</a>' +
                    '                                  <p><strong>Price Range: </strong><span class="text-danger">' + product_price + '</span></p>' +
                    '                               </div>' +
                    '                            </div>';

                $("#show-wishlist-items").append(product_str);
                $(".show-all-wishlist").append(product_str);

                wish_list.push(product_str);
                pushToStorage(wish_list);
                productId.push(product_id) //this is for check the product id in array
                pushToStorageWishID(productId);
                count_items_in_wishlist_update();
                $("#show-wishlist-items").empty();
                $(".show-all-wishlist").empty();
                loadExisting();


                //notification
                toastr.success('Added To Wishlist', toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "30000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });

            }

        }



    });


    $("#show-wishlist-items").on("click", ".w-premove", function () {
        var array_index = parseInt($(this).attr("wpid"));
        $("#list_id_" + array_index).remove();
        delete wish_list[array_index];
        localStorage.removeItem("wish_list");
        pushToStorage(wish_list);
        count_items_in_wishlist_update();
        $("#show-wishlist-items").empty();
        loadExisting();

        toastr.error('Removed From Wishlist', toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });


    });


    $(".show-all-wishlist").on("click", ".w-premove", function () {
        var array_index = parseInt($(this).attr("wpid"));
        $("#list_id_" + array_index).remove();
        delete wish_list[array_index];
        localStorage.removeItem("wish_list");
        pushToStorage(wish_list);
        count_items_in_wishlist_update();
        $(".show-all-wishlist").empty();
        loadExisting();


        toastr.error('Removed From Wishlist', toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });


    });



});

//Validation against the amount of product being added
var count_wishlist = 0;
function count_items_in_wishlist_update() {
    count_wishlist = 0;
    var stored_wishList = JSON.parse(localStorage.getItem("wish_list"));
    wish_list.forEach(countFunc)
    $("#listitem").html(count_wishlist);

    // empty wishlist image
    var empty_wishlist_img = $('.empty_wishlist_img').val();
    var wishlists_url = $('.wishlists-index').val();

    if (count_wishlist == 0) {
        $("#show-wishlist-items").html('<img src="' + empty_wishlist_img + '" class="img-fluid" alt="empty cart"/>');
        $(".show-all-wishlist").html('<img src="' + empty_wishlist_img + '" class="img-fluid" alt="empty cart"/>');
    } else {
        $("#show-all-wishlist").empty();
        $("#show-all-wishlist").html('<a href="' + wishlists_url + '" class="btn btn-white f-s-16 bg-white">Show All Wishlist</a>');
    }

}
function countFunc(item, index) {
    if (item != null) {
        count_wishlist++;
    }
}

/**
 * Store Wishlist Data At Auth
 */


$(document).ready(function () {
    var stored_wishList = JSON.parse(localStorage.getItem("wish_list"));
    
    if (typeof stored_wishList != 'undefined' && stored_wishList != null) {
        const values = Object.values(stored_wishList);

        if (values != null) {
            values.forEach(addTOWishlistInAuth);
            localStorage.removeItem('wish_id');
        }
    }
})

function addTOWishlistInAuth(item, index) {

    var auth_check = $('.auth-check').val();
    if (auth_check == 1) {
        var t = $(".list_id_" + index).val();
        addToWishlist(t);
        localStorage.removeItem('wish_list');
    }

}
