<div class="ps-section__content">
  <ul class="ps-section__links mt-4">
    <li>
      <a href="{{ route('customer.orders') }}">Mis Compras</a>
    </li>
    <li>
      <a href="{{ route('customer.index') }}">Mi perfil</a>
    </li>
    @if ( $userType == 'Vendor' )
    <li>
      <a href="{{ route('seller.dashboard') }}"
        target="_blank">
        Mi Tienda
      </a>
    </li>
    @endif
    <li>
    <a href="{{ route('logout') }}"
      onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
      Salir
    </a>
    <form id="logout-form"
      action="{{ route('logout') }}" 
      method="POST" 
      style="display: none;">
      @csrf
    </form>
    </li>
  </ul>
</div>