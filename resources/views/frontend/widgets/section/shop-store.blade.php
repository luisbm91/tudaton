<div class="p-30">
  <div class="container">
    <div class="row">
      <div class="col-8">
        <div class="ps-section__header">
          <h3 class="text-capitalize">@translate(Shop by Store)</h3>
        </div>
      </div>
      <div class="col-4 text-right">
        <a href="{{ route('vendor.shops') }}" class="h3">
          View All
        </a>
      </div>
    </div>
    <div class="ps-section__content">
      <div class="ps-block--categories-tabs ps-tab-root store_section">
        <div class="ps-tabs">
          <div class="ps-tabs">
            <div class="ps-tab active p-0">
              <div class="row">
                @php
                  $usersStore = App\User::select('id')->where('user_type','Vendor')->latest()->paginate(paginate());
                @endphp

                @foreach ($usersStore as $userStore)
                  @php
                    $store = App\Vendor::where('user_id', $userStore->id)->first();
                  @endphp

                  @if ($store)
                  <div class="col-md-3 col-xl-2 t-mb-30">
                    <a href="{{ route('vendor.shop', $store->slug) }}"
                      class="product-card store-product-card">
                      @if (!$store->shop_logo)
                        <span class="product-card__img-wrapper store-product-card__img-wrapper">
                          <img src="{{asset('vendor-store.jpg')}}"
                            alt="{{ $store->shop_name }}"
                            class="img-fluid mx-auto" />
                        </span>
                      @else
                        <span class="product-card__img-wrapper store-product-card__img-wrapper">
                          <img src="{{ asset($store->shop_logo) }}"
                            alt="{{ $store->shop_name }}"
                            class="img-fluid mx-auto" />
                        </span>
                      @endif
                      <span class="product-card__body">
                        <span class="product-card__title text-center">
                          {{ $store->shop_name }}
                        </span>
                      </span>
                    </a>
                  </div>
                  @endif
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>