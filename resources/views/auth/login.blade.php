@extends('frontend.master')

@section('title')
  Ingresa a tu cuenta
@endsection

@section('content')
  <div class="pt-5" style="background-color: #f1f1f1;">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 mb-3">
          <div class="card card-primary card-outline">
            <div class="card-body" id="sign-in">
              <form method="POST" action="{{ route('login') }}">
                @csrf
                <h3 class="text-center pt-3 pb-4">Ingresa a tu cuenta</h3>
                <div class="form-group">
                  <input type="email"
                    name="email"
                    class="form-control @error('email') is-invalid @enderror"
                    placeholder="E-mail"
                    value="{{ @old('email') }}"
                    maxlength="160"
                    required />
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
                <div class="form-group form-forgot">
                  <input type="password"
                    name="password"
                    class="form-control @error('password') is-invalid @enderror"
                    placeholder="Contraseña"
                    minlength="6" maxlength="30"
                    required />
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="form-group">
                  <div class="ps-checkbox">
                    <input class="form-control" type="checkbox" id="remember-me" name="remember">
                    <label for="remember-me">Recordarme</label>
                  </div>
                </div>
                <div class="form-group submtit">
                  <button type="submit"
                    class="ps-btn btn-block">
                    Quiero Ingresar
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="col-12 col-md-8 m-auto text-center">
            <h3 class="mb-5">Aun no tienes una cuenta?</h3>
            <a href="{{ route('register', ['type' => 'customer']) }}"
              class="btn ps-btn btn-block">
              Regístrate aquí
            </a>
            <p class="text-center mt-5">
              <a href="{{ route('password.reset') }}">
                Olvidaste tu Contraseña?
              </a>
            </p>
          </div>
        </div>
      </div>

      {{--
      <div class="ps-form__footer">
        <p>@translate(Connect with):</p>
        <ul class="ps-list--social">
          @if(!env('FACEBOOK_CLIENT_ID') == "" && !env('FACEBOOK_SECRET') == "" && !env('FACEBOOK_CALLBACK') == "")
          
              <li><a class="facebook" href="{{ url('/auth/redirect/facebook') }}"><i
                              class="fa fa-facebook"></i></a></li>
          @endif

          @if(!env('GOOGLE_CLIENT_ID') == "" && !env('GOOGLE_CALLBACK') == "" && !env('GOOGLE_SECRET') == "")
              <li><a class="google" href="{{ url('/auth/redirect/google') }}"><i class="fa fa-google-plus"></i></a></li>
          @endif
        </ul>
      </div>
      --}}
    </div>
  </div>
@endsection