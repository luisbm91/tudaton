<div class="ps-panel--sidebar" id="search-sidebar">
    <div class="ps-panel__header">

        <form class="ps-form--quick-search" action="javascript:void()" method="get">
                    
                    <input class="form-control w-65" id="mobile_filter_input" type="text" placeholder="I'm shopping for...">
                    <div class="form-group--icon w-40"><i class="icon-chevron-down"></i>
                        <input type="hidden" id="mobile_filter_url" value="{{ route('header.search') }}">

                        <select class="form-control" name="filter_type" id="mobile_filter_type">
                            <option value="product" selected>@translate(Product)</option>
                            @if(vendorActive())
                            <option value="shop">@translate(Shop)</option>
                            @endif
                        </select>
                    </div>
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </form>

                {{-- Search result --}}
                    {{-- <table class="table search-table" id="mobile_show_data">
                       
                    </table> --}}

                     {{-- Search result --}}
                    <div class="search-table d-none">
                        <div class="row m-auto p-3" id="mobile_show_data">
                            {{-- Data goes here --}}
                        </div>
                    </div>
                {{-- Search result:END --}}

                {{-- Search result:END --}}


    </div>
    <div class="navigation__content"></div>
</div>
